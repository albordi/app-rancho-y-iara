import { useState, useEffect, useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { GlobalStyles } from '../constants/styles';
import { Label1 } from './UI/Labels';
import { DeliveryContext } from '../store/context/deliverycontext';

function DropDownInput({ label, itemsToShow, onSelectItemHandle }) {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([]);

  // console.log('DropDownInput ',itemsToShow);

  useEffect(() => {
    const newItems = [];
    let i = 0;
    itemsToShow.map((product) => {
      newItems.push({ label: product.name, value: product.name });
      i++;
      return;
    });
    setItems(newItems);
  }, [itemsToShow]);

  // useEffect(() => {
  //   onSelectItemHandle(value);
  // }, value);

  // console.log(items);

  // console.log('[DropDownInput] products',itemsToShow);

  return (
    <View>
      {/* <Label1 style={{ textAlign: 'center' }}>{label}</Label1> */}
      <DropDownPicker
        style={styles.dropdown}
        zIndex={3000}
        zIndexInverse={1000}
        // containerStyle={{
        //   borderWidth: 1,
        // }}
        maxHeight={300}
        open={open}
        value={value}
        items={items}
        setOpen={setOpen}
        setValue={setValue}
        setItems={setItems}
        autoScroll={false}
        onChangeValue={(value) => {
          onSelectItemHandle(value)
        }}
        placeholder='Selecione um item'
        placeholderStyle={{
          color: GlobalStyles.colors.secondary,
          fontWeight: 'bold',
        }}
        dropDownContainerStyle={{
          borderBottomColor: GlobalStyles.colors.tertiary,
          borderBottomWidth: 1,
          zIndex: 1000,
          backgroundColor: 'white',
        }}
      />
    </View>
  );
}

export default DropDownInput;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
  dropdown: {
    borderWidth: 0,
    marginTop: 5,
    backgroundColor: 'white',
    borderBottomColor: GlobalStyles.colors.tertiary,
    borderBottomWidth: 2,
    zIndex: 1000,
    elevation: 1000,
  },
});
