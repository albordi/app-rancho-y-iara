import { useState, ActivityIndicator } from "react";

function UploadImageToFirestore() {

  const [url, setUrl] = useState();
  const [upLoading, setUploading] = useState(false);
  
  const uploadImageToFirestoreStorage = async ( uri ) => {
    console.log('[Upload image] 1');
    const response = await fetch(uri);
    const blob = await response.blob();
    const receiptRef = firebase
      .storage()
      .ref()
      .child(`pic/image`);
    // setIsLoading(true);
    receiptRef
      .put(blob)
      .then((response) => {
        receiptRef
          .getMetadata()
          .then((receiptMetadata) => {
            receiptRef
              .getDownloadURL()
              .then((url) => {
                // updatePayment(receiptUrl, receiptMetadata);
                setUrl(url)
                console.log(url);
              })
              .catch((error) => {
                console.log('Erro =>', error);
                Alert.alert(
                  'Erro ao recuperar o endereço da imagem do comprovante do banco de dados!',
                  error
                );
              });
          })
          .catch((error) => {
            Alert.alert(
              'Erro ao recuperar os metadados do comprovante de pagamento!',
              error
            );
          });
      })
      .catch((error) => {
        Alert.alert('Erro ao armazenar a imagem do comprovante!', error);
      });
  };

  console.log('[Upload Image To Firebase]', url);

  return(
    <View>
      {uploadImageToFirestoreStorage}
      <Text>UploadImage to Firestore</Text>
      {!uploading ? null : <ActivityIndicator size={'small'} color='black' />}
    </View>
  )
}

export default UploadImageToFirestore