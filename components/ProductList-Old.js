import { useEffect, useState } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  Platform,
  Pressable,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Checkbox from 'expo-checkbox';

import { GlobalStyles } from '../constants/styles';

function ProductList({ products, onUpdateProductHandle }) {
  // console.log('[Product List Component]');
  const [productsAux, setProductsAux] = useState([]);
  const [isChecked, setChecked] = useState(false);

  const navigation = useNavigation();

  useEffect(() => {
    const newProducts = [];
    products.map((product) => {
      newProduct = { ...product, isChecked: false };
      newProducts.push(newProduct);
      return;
    });
    setProductsAux(newProducts);
  }, []);

  const onSelectProductHandler = (id) => {
    // console.log('[Product List] Pressed checkbox', id);
    // console.log(productsAux[0]);
    // const index = productsAux.indexOf(id === productsAux.id );
    const index = productsAux.map((object) => object.id).indexOf(id);
    // const newProduct = {...productsAux[index].isChecked = true};
    productsAux[index].isChecked = !productsAux[index].isChecked;
    // console.log(
    //   '[Product List] Pressed checkbox',
    //   index,
    //   '-',
    //   productsAux[index]
    // );
    setChecked(!isChecked);
    // console.log(isChecked);
  };

  function renderProductItem(itemData) {
    // console.log('[Product List component]', JSON.stringify(itemData, null, 2));
    return (
      <View style={styles.productContainer}>
        <Pressable onPress={() => onUpdateProductHandle(itemData.item.id)}>
          <View style={styles.details}>
            <Image
              style={styles.image}
              source={{ uri: itemData.item.imageUrl }}
            />
            <View>
              <Text style={styles.productName}>
                {itemData.item.name.toUpperCase()}
              </Text>
              <Text style={styles.productInformation}>
                Quantidade Máxima: {itemData.item.availableQuantity}
              </Text>

            </View>
          </View>
        </Pressable>
        <View style={styles.checkboxContainer}>
          <Checkbox
            style={styles.checkbox}
            value={itemData.item.isChecked}
            onValueChange={() => onSelectProductHandler(itemData.item.id)}
            color={
              itemData.item.isChecked
                ? GlobalStyles.colors.secondary
                : undefined
            }
          />
        </View>
      </View>
    );
  }

  return (
    <View>
      <FlatList
        style={styles.flatListContainer}
        data={productsAux}
        renderItem={renderProductItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

export default ProductList;

const styles = StyleSheet.create({
  productContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 4,
    // paddingHorizontal: 10,
    paddingVertical: 2,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  details: {
    width: '70%',
    flexDirection: 'row',
  },
  productName: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  productInformation: {
    fontSize: 10,
    paddingHorizontal: 15,
    // backgroundColor: 'red'
  },
  checkboxContainer: {
    flex: 1,
    paddingHorizontal: 15,
    // backgroundColor: 'red',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  flatListContainer: {
    flexGrow: 1,
    // height: '65%',
  },
  image: {
    width: 60,
    height: '100%',
  },
});
