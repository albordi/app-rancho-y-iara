// Custom text or number input
import { TextInput, StyleSheet, View } from 'react-native';
import { GlobalStyles } from '../constants/styles';

function Input({ style, label, value, isValid, textInputConfig }) {
  // console.log('[input]', isValid);
  return (
    <View style={styles.inputContainer}>
      <TextInput
        value={value}
        style={[styles.input, style, !isValid && styles.invalidInput]}
        {...textInputConfig}
      />
    </View>
  );
}

export default Input;

const styles = StyleSheet.create({
  inputContainer: {
    // marginHorizontal: 4,
    // marginVertical: 8,
    flex: 1,
  },
  input: {
    // height: 50,
    // width: 300,
    padding: 6,
    borderBottomColor: '#ddb52f',
    borderBottomWidth: 2,
    color: GlobalStyles.colors.textColor,
    // marginVertical: 8,
    fontSize: 16,
    // fontWeight: 'bold',
    // textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 6,
  },
  invalidLabel: {
    color: GlobalStyles.colors.error,
  },
  invalidInput: {
    backgroundColor: GlobalStyles.colors.error,
    opacity: 0.5,
  },
});
