import { useEffect, useState } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  Platform,
  Pressable,
  TextInput,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { useNavigation } from '@react-navigation/native';

import { GlobalStyles } from '../constants/styles';

function ProductList({
  products,
  onChangeMaxProdHandle,
}) {
  console.log('[Product List Component]');
  // console.log('[Product List Component]', products);
  const [productsAux, setProductsAux] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    const newProducts = [];
    products.map((product) => {
      newProduct = { ...product };
      newProducts.push(newProduct);
      return;
    });
    setProductsAux(newProducts);
  }, []);

  const onChangeQuantityHandler = (productId, value) => {
    const newProductsAux = [...productsAux];
    const index = newProductsAux.findIndex((obj) => obj.id === productId);
    console.log(newProductsAux[index].availableProducts);
    newProductsAux[index].availableProducts =
      newProductsAux[index].availableProducts + value;
    if (newProductsAux[index].availableProducts < 0) {
      newProductsAux[index].availableProducts = 0;
      return;
    }
    setProductsAux(newProductsAux);
    onChangeMaxProdHandle(productId, newProductsAux[index].availableProducts);
  };

  function renderProductItem(itemData) {
    // console.log(
    //   '[Product List component] item data',
    //   JSON.stringify(itemData, null, 2)
    // );
    // console.log(
    //   '[Product List component] item data available prods',
    //   itemData.item.availableProducts
    // );
    return (
      <View style={styles.productContainer}>
        <View style={styles.details}>
          <Image
            style={styles.image}
            source={{ uri: itemData.item.imageUrl }}
          />
          <View>
            <Text style={styles.productName}>
              {itemData.item.name.toUpperCase()}
            </Text>
          </View>
        </View>
        <View style={styles.quantityContainer}>
          <View style={styles.textInputContainer}>
            <Ionicons
              name='remove'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => onChangeQuantityHandler(itemData.item.id, -1)}
            />
            <Text style={styles.textInput}>
              {itemData.item.availableProducts.toString()}
            </Text>
            {/* <TextInput
              style={styles.textInput}
              // label='E-mail'
              // placeholder='e-mail'
              value={itemData.item.availableProducts.toString()}
              onChangeText={(value) => onChangeQuantityHandler(itemData.item.id, value)}
            /> */}
            <Ionicons
              name='add'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => onChangeQuantityHandler(itemData.item.id, 1)}
            />
          </View>
        </View>
      </View>
    );
  }

  return (
    <View>
      <FlatList
        style={styles.flatListContainer}
        data={productsAux}
        renderItem={renderProductItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

export default ProductList;

const styles = StyleSheet.create({
  productContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 4,
    // paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  details: {
    width: '70%',
    flexDirection: 'row',
  },
  productName: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  productInformation: {
    fontSize: 10,
    paddingHorizontal: 15,
    // backgroundColor: 'red'
  },
  quantityContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    // paddingHorizontal: 10,
  },
  textInputContainer: {
    // flex: 1,
    // paddingHorizontal: 15,
    // borderWidth: 1,
    // width: 10,
    // borderColor: 'black',
    alignItems: 'center',
    flexDirection: 'row',
    textAlign: 'left',
    justifyContent: 'flex-end',
  },
  textInput: {
    textAlign: 'center',
    color: GlobalStyles.colors.secondary,
    fontSize: 20,
  },
  flatListContainer: {
    flexGrow: 1,
    // height: '65%',
  },
  image: {
    width: 60,
    height: '100%',
  },
});
