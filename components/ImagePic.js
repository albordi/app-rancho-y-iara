import { useState } from 'react';
import {
  View,
  StyleSheet,
  Button,
  Pressable,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Ionicons } from '@expo/vector-icons';
// import { firebase } from '../firebaseConfig';
import { storage } from '../firebaseConfig';
import { ref } from 'firebase/storage';
import firebase from 'firebase/compat/app';

function ImagePic( props ) {
  // const [imageUri, setImageUri] = useState(null);
  const [uploading, setUploading] = useState(false);
  const [image, setImage] = useState();

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: 'Images', // We can specify whether we need only Images or Videos
      allowsEditing: true,
      aspect: [2, 2],
      quality: 0.5, // 0 means compress for small size, 1 means compress for maximum quality
      selectionLimit: 1,
    });
    // console.log('[pickImage image get]', result);

    const imgUri = result.assets[0].uri;
    setImage(imgUri);
    console.log(props);
    props.onImagePicker(imgUri);
  };

  const launchImageGalery = async () => {
    console.log('[ImagePicker] launchGalery');
    const options = {
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      // allowsEditing: true,
      // aspect: [16, 9],
      quality: 0.5,
    };
    const image = await ImagePicker.launchImageLibraryAsync(options);
    image.type = 'image';
    console.log(image.assets[0].uri);
    setImage(image.assets[0].uri);
    props.onImagePicker(image.assets[0].uri);
  };

  return (
    <View style={styles.container}>
      {image && (
        <Image source={{ uri: image }} style={{ width: 100, height: 100 }} />
      )}
      <Pressable onPress={launchImageGalery}>
        <Ionicons name='cloud-upload' size={24} color='black' />
      </Pressable>
      {/* <Button title='Select Image' onPress={pickImage} /> */}
      {/* {!uploading ? (
        <Button title='Upload Image' onPress={uploadImage} />
      ) : (
        <ActivityIndicator size={'small'} color='black' />
      )} */}
      {!uploading ? null : <ActivityIndicator size={'small'} color='black' />}
    </View>
  );
}

export default ImagePic;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

/*
const uploadImage = async () => {
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function () {
        reject(new TypeError('Network request failed'));
      };
      xhr.responseType = 'blob';
      xhr.open('GET', image, true);
      xhr.send(null);
    });
    const ref = firebase.storage().ref().child(`Pictures/Image1`);
    const snapshot = ref.put(blob);
    snapshot.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      () => {
        setUploading(true);
      },
      (error) => {
        setUploading(false);
        console.log(error);
        blob.close();
        return;
      },
      () => {
        snapshot.snapshot.ref.getDownloadURL().then((url) => {
          setUploading(false);
          // console.log('Download URL: ', url);
          setImage(url);
          blob.close();
          return url;
        });
      }
    );
  };

  */
