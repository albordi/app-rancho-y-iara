import { Text, StyleSheet } from "react-native";
import { GlobalStyles } from "../../constants/styles";

export function Label1(props) {
  return (
      <Text style={[styles.textLabel,props.style]}>{props.children}</Text>
  );
}

export function Label2(props) {
  return (
      <Text style={[styles.textLabel2,props.style]}>{props.children}</Text>
  );
}

const styles = StyleSheet.create({
  textLabel: {
    // fontFamily: 'Roboto_200Thin',
    fontSize: 20,
    // color: 'black',
    color: GlobalStyles.colors.secondary,
    // textAlign: "left",
  },
  textLabel2:{
     fontSize: 15,
     color: GlobalStyles.colors.secondary,
     textAlign: 'left',
     marginLeft: 20,
  }
});
