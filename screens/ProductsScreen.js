import { useState, useEffect, useLayoutEffect } from 'react';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  Pressable,
  Image,
  RefreshControl,
  StyleSheet,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { getCollection } from '../util/firebase';
import Spinner from '../components/Spinner';
import { GlobalStyles } from '../constants/styles';
import { Label2 } from '../components/UI/Labels';

function ProductsScreen({ navigation }) {
  console.log('[Products Screen] started');
  console.log('[Products Screen] products', products);

  const [isFetchingProducts, setIsFetchingProducts] = useState(true);
  const [error, setError] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [products, setProducts] = useState([]);

  async function getProducts() {
    setIsFetchingProducts(true);
    try {
      const prods = await getCollection('products');
      setProducts(prods);
    } catch (error) {
      setError('Houve um erro ao carregar os produtos !!!');
    }
    setIsFetchingProducts(false);
  }

  useLayoutEffect(() => {
    getProducts();
  }, []);

  function onAddProductHandle() {
    // console.log('[Create Delivery Screen] Add new product');
    navigation.navigate('ManageProductsScreen');
  }

  const onUpdateProductHandle = (productId) => {
    console.log('[Manage Delivery Screen] update product pressed', productId);

    navigation.navigate('ManageProductsScreen', {
      productId: productId,
    });
  };

  function renderProduct(product) {
    console.log(
      '[Product Screen component] item data',
      JSON.stringify(product, null, 2)
    );
    return (
      <ScrollView
        style={styles.productContainer}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => {
              setIsFetchingProducts(true);
              getProducts();
              setIsFetchingProducts(false);
            }}
          />
        }
      >
        <Pressable onPress={() => onUpdateProductHandle(product.item.id)}>
          <View style={styles.details}>
            <Image
              style={styles.image}
              source={{ uri: product.item.imageUrl }}
            />
            <Text style={styles.productName}>
              {product.item.name.toUpperCase()}
            </Text>
            <Text style={styles.productPrice}>
              {product.item.price.toFixed(2)}
            </Text>
          </View>
        </Pressable>
      </ScrollView>
    );
  }

  return (
    <View>
      <View style={styles.iconContainer}>
        <Pressable onPress={onAddProductHandle}>
          <Ionicons
            name='add-circle'
            size={24}
            color={GlobalStyles.colors.secondary}
          />
        </Pressable>
      </View>
      <Label2>Clique sobre o produto para alterar</Label2>
      {isFetchingProducts ? (
        <Spinner />
      ) : (
        <FlatList
          style={styles.flatListContainer}
          data={products}
          renderItem={renderProduct}
          keyExtractor={(item) => item.id}
        />
      )}
    </View>
  );
}

export default ProductsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
  },
  productContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 4,
    // paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  details: {
    width: '70%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  productName: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  productPrice: {
    textAlign: 'right',
  },
  productInformation: {
    fontSize: 10,
    paddingHorizontal: 15,
    // backgroundColor: 'red'
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginHorizontal: 20,
    zIndex: -1,
  },
  // textInputContainer: {
  //   alignItems: 'center',
  //   flexDirection: 'row',
  //   textAlign: 'left',
  //   justifyContent: 'flex-end',
  // },
  // textInput: {
  //   textAlign: 'center',
  //   color: GlobalStyles.colors.secondary,
  //   fontSize: 20,
  // },
  flatListContainer: {
    flexGrow: 1,
    // height: '65%',
  },
  image: {
    width: 60,
    height: '100%',
  },
});
