import { useState, useContext, useEffect } from 'react';
import { FlatList, StyleSheet, View, Text } from 'react-native';

import { DeliveryContext } from '../store/context/deliverycontext';

import DeliveryGridTile from '../components/DeliveryGridTile';
import PrimaryButton from '../components/PrimaryButton';
import { GlobalStyles } from '../constants/styles';

function DeliveryScreen({ navigation }) {
  const [deliveries, setDeliveries] = useState([]);
  const deliveryCtx = useContext(DeliveryContext);

  console.log('[Deliveries Screen]');

  useEffect(() => {
    setDeliveries(deliveryCtx.deliveries);
  }, []);

  console.log(deliveries);

  function renderDeliveryItem(itemData) {
    function pressHandler() {
      navigation.navigate('ManageDeliveryScreen', {
        deliveryId: itemData.item.id,
      });
    }

    return (
      <DeliveryGridTile
        title={itemData.item.deliveryDate}
        color={GlobalStyles.colors.tertiary}
        onPress={pressHandler}
      ></DeliveryGridTile>
    );
  }

  const buttonPressHandler = () => {
    console.log('Button pressed');
    navigation.navigate('ManageDeliveryScreen');
  };

  return (
    <View style={styles.container}>
      <View style={styles.buttonContainer}>
        <PrimaryButton onPress={buttonPressHandler}>
          Criar nova entrega
        </PrimaryButton>
      </View>
      <FlatList
        data={deliveries}
        keyExtractor={(item) => item.id}
        renderItem={renderDeliveryItem}
      ></FlatList>
    </View>
  );
}

export default DeliveryScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  buttonContainer: {
    // flex: 1,
    // marginTop: 100,
    padding: 16,
  },
  textInputContainer: {
    marginHorizontal: 10,
    alignItems: 'center',
  },
});
