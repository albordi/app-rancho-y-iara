import { useContext, useLayoutEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  Pressable,
  StyleSheet,
} from 'react-native';
import { UserContext } from '../../store/context/usercontext';
import { GlobalStyles } from '../../constants/styles';
import UserCard from '../../components/UserCard';

function UsersScreen({ navigation }) {
  console.log('[UsersScreen rendering...]');
  const [isUpdating, setIsUpdating] = useState(false);

  const usersCtx = useContext(UserContext);
  useLayoutEffect(() => {
    usersCtx.fetchUsers();
  }, []);

  function onUpdateUserHandle(user) {
    console.log('[Users Screen] handle update user', user);
    navigation.navigate('UserScreen', { user: user });
  }

  async function onDeleteUserHandle(userId) {
    try{
      setIsUpdating(true);
      await usersCtx.deleteUser(userId);
      console.log('[UsersScreen] User Deleted', userId)
    } catch(error){
      console.log('[UsersScreen] Error to delete user', userId)
    }
    setIsUpdating(false);
    console.log('[Users Screen] handle delete user');
    // navigation.navigate('UserScreen', {user: user})
  }

  function onUpdateUserHandle(userId) {
    console.log('[Users Screen] handle delete user');
    // navigation.navigate('UserScreen', {user: user})
  }

  function renderUser(itemData) {
    // console.log('item', itemData);
    return (
        <UserCard
          user={itemData.item}
          onDeleteUserHandle={onDeleteUserHandle}
          onUpdateUserHandle={onUpdateUserHandle}
        />
    );
  }

  return (
    // <ScrollView style={styles.usersScreenContainer}>
    <View style={styles.usersScreenContainer}>
      {/* <Text>Users Screen</Text> */}
      <FlatList
        // style={styles.flatListContainer}
        data={usersCtx.users}
        renderItem={renderUser}
        keyExtractor={(item) => item.id}
      />
    </View>
    // </ScrollView>
  );
}

export default UsersScreen;

const styles = StyleSheet.create({
  usersScreenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.primary,
  },
});
