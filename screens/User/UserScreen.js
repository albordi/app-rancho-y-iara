import { useState, useContext, useEffect } from 'react';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  Pressable,
  StyleSheet,
  KeyboardAvoidingView,
} from 'react-native';
import { UserContext } from '../../store/context/usercontext';
import { GlobalStyles } from '../../constants/styles';
import UserCard from '../../components/UserCard';
import PrimaryButton from '../../components/PrimaryButton';
import Input from '../../components/Input';

function UserScreen({ route }) {
  const { user } = route.params;
  console.log('[User screen]', user);

  const [userInputs, setUserInputs] = useState({
    name: { value: user.name || '', isValid: true },
    email: { value: user.email || '', isValid: true },
    city: { value: user.city || 'Campinas', isValid: true },
    phone: { value: user.phone || '19981380000', isValid: true },
    password: { value: 'Bordi1234', isValid: true },
    passwordConfirm: { value: 'Bordi1234', isValid: true },
  });

  // name: { value: user.name || '', isValid: true },
  // email: { value: user.email || '', isValid: true },
  // city: { value: user.city || 'Campinas', isValid: true },
  // phone: { value: user.phone || '19981380000', isValid: true },
  // password: { value: 'Bordi1234', isValid: true },
  // passwordConfirm: { value: 'Bordi1234', isValid: true },

  function nameChangeHandler(value) {
    console.log('Name changed', value);
    const newUser = { ...userInputs, name: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function emailChangeHandler(value) {
    console.log('Email changed', value);
    const newUser = { ...userInputs, email: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function cityChangeHandler(value) {
    console.log('City changed', value);
    const newUser = { ...userInputs, city: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function phoneChangeHandler(value) {
    console.log('Phone changed', value);
    const newUser = { ...userInputs, phone: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function passwordChangeHandler(value) {
    console.log('Password changed');
    const newUser = {
      ...userInputs,
      password: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function passwordConfirmChangeHandler(value) {
    console.log('Password changed');
    const newUser = {
      ...userInputs,
      password: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function onHandleUpdate() {
    console.log('[UserScreen handle update user]');
  }

  return (
    <View style={styles.userScreenContainer}>
      <KeyboardAvoidingView behavior='height' style={styles.container}>
        <View style={styles.fieldContainer}>
          <Input
            label='Nome'
            value={userInputs.name.value}
            isValid={userInputs.name.isValid}
            textInputConfig={{
              onChangeText: nameChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='E-mail'
            value={userInputs.email.value}
            isValid={userInputs.email.isValid}
            textInputConfig={{
              onChangeText: emailChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='Telefone'
            value={userInputs.phone.value}
            isValid={userInputs.phone.isValid}
            textInputConfig={{
              onChangeText: phoneChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='Cidade'
            value={userInputs.city.value}
            isValid={userInputs.city.isValid}
            textInputConfig={{
              onChangeText: cityChangeHandler,
            }}
          />
        </View>
        {/* <View style={styles.fieldContainer}>
          <Input
            label='Password'
            value={userInputs.password.value}
            isValid={userInputs.password.isValid}
            textInputConfig={{
              onChangeText: passwordChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='Confirme sua password'
            value={userInputs.passwordConfirm.value}
            isValid={userInputs.passwordConfirm.isValid}
            textInputConfig={{
              onChangeText: passwordConfirmChangeHandler,
            }}
          />
        </View> */}
        {/* <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <PrimaryButton onPress={onHandleUpdate}>Atualizar</PrimaryButton>
          </View>
        </View> */}
      </KeyboardAvoidingView>
    </View>
  );
}

export default UserScreen;

const styles = StyleSheet.create({
  userScreenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.primary,
  },
  fieldContainer: {
    flexDirection: 'row',
    width: '80%',
    marginHorizontal: 20,
  },
});
