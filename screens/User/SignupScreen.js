import { useState, useContext } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { isValidEmail, isValidPhoneNumber } from '../../util/validation';
import Input from '../../components/Input';
import PrimaryButton from '../../components/PrimaryButton';
import { GlobalStyles } from '../../constants/styles';
import { AuthContext } from '../../store/context/authcontext';
import Spinner from '../../components/Spinner';

function SignupScreen({ navigation }) {
  console.log('[SignUp screen]');
  const [userInputs, setUserInputs] = useState({
    name: { value: 'Ana', isValid: true },
    email: { value: 'albordignon@gmail.com', isValid: true },
    city: { value: 'Campinas', isValid: true },
    phone: { value: '19981380000', isValid: true },
    password: { value: 'Bordi1234', isValid: true },
    passwordConfirm: { value: 'Bordi1234', isValid: true },
  });
  const [isAuthenticating, setIsAuthenticating] = useState(false);
  const [error, setError] = useState(null);
  const [showPassword, setShowPassword] = useState(false);

  const authCtx = useContext(AuthContext);
  
  console.log('[SignUp Screen isAuthenticated]', authCtx.isAuthenticated);

  // console.log('[SignUp Screen line 25]', JSON.stringify(userInputs, null, 2));

  function nameChangeHandler(value) {
    console.log('Name changed', value);
    const newUser = { ...userInputs, name: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function emailChangeHandler(value) {
    console.log('Email changed', value);
    const newUser = { ...userInputs, email: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function cityChangeHandler(value) {
    console.log('City changed', value);
    const newUser = { ...userInputs, city: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function phoneChangeHandler(value) {
    console.log('Phone changed', value);
    const newUser = { ...userInputs, phone: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function passwordChangeHandler(value) {
    console.log('Password changed');
    const newUser = {
      ...userInputs,
      password: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function passwordConfirmChangeHandler(value) {
    console.log('Password changed');
    const newUser = {
      ...userInputs,
      password: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  async function signupHandler() {
    console.log('[SignUp Screen] Button pressed');
    setIsAuthenticating(true);
    const userEmailIsValid = isValidEmail(userInputs.email.value);
    const userPasswordIsValid = userInputs.password.value.length > 5;
    const userNameIsValid = userInputs.name.value.length >= 1;
    const userPhoneIsValid = isValidPhoneNumber(userInputs.phone.value);
    console.log('Validação phone linha 70', userPhoneIsValid);
    const userCityIsValid = userInputs.city.value.length >= 2;
    console.log(
      userEmailIsValid,
      userPasswordIsValid,
      userNameIsValid,
      userPhoneIsValid,
      userCityIsValid
    );
    if (
      !userNameIsValid ||
      !userEmailIsValid ||
      !userPasswordIsValid ||
      !userPhoneIsValid ||
      !userCityIsValid
    ) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setUserInputs({
        name: { value: userInputs.name.value, isValid: userNameIsValid },
        email: { value: userInputs.email.value, isValid: userEmailIsValid },
        phone: { value: userInputs.phone.value, isValid: userPhoneIsValid },
        city: { value: userInputs.city.value, isValid: userCityIsValid },
        password: {
          value: userInputs.password.value,
          isValid: userPasswordIsValid,
        },
        passwordConfirm: {
          value: userInputs.passwordConfirm.value,
          isValid: userPasswordIsValid,
        },
      });
      setIsAuthenticating(false);
      return;
    }
    if (userInputs.password.value !== userInputs.passwordConfirm.value) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se as senhas foram digitadas corretamente.'
      );
      setIsAuthenticating(false);
      return;
    }
    const userToCreate = {
      name: userInputs.name.value,
      email: userInputs.email.value,
      phone: userInputs.phone.value,
      city: userInputs.city.value,
      password: userInputs.password.value,
    };
    try {
      await authCtx.signupUser(userToCreate);
    } catch (error) {
      console.log('[SignUp Screen] error', error);
      Alert.alert(
        'Erro ao cadastrar o usuário',
        'Verifique se os dados estão corretos.'
      );
    }
    setIsAuthenticating(false);
    console.log(
      '[SignUp Screen] Finalizing creation response -> ',
      isAuthenticating
    );
  }

  const formIsInvalid =
    !userInputs.email.isValid || !userInputs.password.isValid;

  // if (isAuthenticating) {
  //   return <Spinner />;
  // }

  return (
    <ImageBackground
      style={styles.signupScreen}
      source={require('../../assets/screen1.png')}
      resizeMode='stretch'
    >
      <KeyboardAvoidingView behavior='height' style={styles.container}>
        <Text style={styles.title}>Novo Cadastro</Text>

        <Text style={styles.label}>Nome</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='Nome'
            value={userInputs.name.value}
            isValid={userInputs.name.isValid}
            textInputConfig={{
              onChangeText: nameChangeHandler,
            }}
          />
        </View>
        <Text style={styles.label}>E-mail</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='E-mail'
            value={userInputs.email.value}
            isValid={userInputs.email.isValid}
            textInputConfig={{
              onChangeText: emailChangeHandler,
            }}
          />
        </View>
        <Text style={styles.label}>Telefone</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='Telefone'
            value={userInputs.phone.value}
            isValid={userInputs.phone.isValid}
            textInputConfig={{
              onChangeText: phoneChangeHandler,
            }}
          />
        </View>
        <Text style={styles.label}>Cidade</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='Cidade'
            value={userInputs.city.value}
            isValid={userInputs.city.isValid}
            textInputConfig={{
              onChangeText: cityChangeHandler,
            }}
          />
        </View>
        <Text style={styles.label}>Password</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='Password'
            value={userInputs.password.value}
            isValid={userInputs.password.isValid}
            textInputConfig={{
              onChangeText: passwordChangeHandler,
              secureTextEntry:!showPassword
            }}
          />
          <Ionicons
              style={{
                position: 'absolute',
                top: 8,
                right: 3,
                zIndex: 1,
                opacity: 0.4,
              }}
              name='eye'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => setShowPassword(!showPassword)}
            />
            {showPassword && (
              <Ionicons
                style={{
                  position: 'absolute',
                  top: 8,
                  right: 3,
                  zIndex: 1,
                  opacity: 0.4,
                }}
                name='eye-off'
                size={24}
                color={GlobalStyles.colors.secondary}
                onPress={() => setShowPassword(!showPassword)}
              />
            )}
        </View>
        <Text style={styles.label}>Confirme a password</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='Confirme sua password'
            value={userInputs.passwordConfirm.value}
            isValid={userInputs.passwordConfirm.isValid}
            textInputConfig={{
              onChangeText: passwordConfirmChangeHandler,
              secureTextEntry:!showPassword
            }}
          />
        </View>

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <PrimaryButton onPress={signupHandler}>Confirme</PrimaryButton>
          </View>
        </View>
        <View styles={styles.buttonContainer}>
          <PrimaryButton
            onPress={() => navigation.navigate('AuthScreen')}
            style={styles.button}
          >
            Tela de login
          </PrimaryButton>
        </View>
        {isAuthenticating && <Spinner />}
        {/* <Image style={styles.logo} source={require('../../assets/logo.png')} /> */}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}

export default SignupScreen;

const styles = StyleSheet.create({
  signupScreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title:{
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 40,
    color: GlobalStyles.colors.secondary,
  },  
  fieldContainer: {
    flexDirection: 'row',
    width: '80%',
    marginHorizontal: 20,
    marginBottom: 15,
  },
  label:{
    color: GlobalStyles.colors.secondary,
    marginHorizontal: 20,
  },  
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 20,
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    backgroundColor: 'transparent',
    color: GlobalStyles.colors.secondary,
  },
});
