import { useState, useContext, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Input from '../../components/Input';
import PrimaryButton from '../../components/PrimaryButton';
import { GlobalStyles } from '../../constants/styles';
import { AuthContext } from '../../store/context/authcontext';
import GLOBALS from '../../Globals';
import Spinner from '../../components/Spinner';
import { isValidEmail } from '../../util/validation';
import { TextInput } from 'react-native-gesture-handler';

function AuthScreen({ navigation }) {
  console.log('[Authscreen] starting...');
  const [user, setUser] = useState({
    email: { value: 'albordignon@gmail.com', isValid: true },
    password: { value: 'Bordi1234', isValid: true },
  });
  const [isAuthenticating, setIsAuthenticating] = useState(false);
  // const [error, setError] = useState(null);
  const [showPassword, setShowPassword] = useState(false);

  const authCtx = useContext(AuthContext);
  // console.log(
  //   '[Authscreen rendered] AuthCTX',
  //   JSON.stringify(authCtx.isAuthenticated, null, 2)
  // );

  useEffect(() => {
    console.log(
      '[Authscreen] useEffect AuthCTX',
      JSON.stringify(authCtx, null, 2)
    );
  }, [authCtx]);

  function emailChangeHandler(value) {
    console.log('Email changed', value);
    const newUser = { ...user, email: { value: value, isValid: true } };
    setUser(newUser);
  }

  function passwordChangeHandler(value) {
    console.log('Password changed');
    const newUser = { ...user, password: { value: value, isValid: true } };
    setUser(newUser);
  }

  // console.log('user', JSON.stringify(user, null, 2));

  const signinHandler = async () => {
    console.log('[AuthScreen] Setting authenticating to true...');
    setIsAuthenticating(true);
    // console.log('[AuthScreen] Login Button pressed', user);
    // console.log('[AuthScreen] isAuthenticating line 44', isAuthenticating);
    const userEmailIsValid = isValidEmail(user.email.value);
    // console.log('[AuthScreen] userEmailIsValid line 44', userEmailIsValid);

    if (!userEmailIsValid) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setUser({
        email: { value: user.email.value, isValid: userEmailIsValid },
        password: {
          value: user.password.value,
          isValid: user.password.isValid,
        },
      });
      console.log('[AuthScreen] userEmailIsValid line 44', userEmailIsValid);
      setIsAuthenticating(false);
      return;
    }
    try {
      await authCtx.signin(user.email.value, user.password.value);
      setIsAuthenticating(false);
    } catch (error) {
      Alert.alert(
        'Falha no login',
        'Verifique se o e-mail e a senha estão corretos.'
      );
    }
    setIsAuthenticating(false);
    // navigation.navigate('ManageDeliveryScreen');
  };

  function signupHandler() {
    console.log('SignUp ');
    navigation.navigate('SignupScreen');
  }

  async function forgetPasswordHandler() {
    const userEmailIsValid = isValidEmail(user.email.value);
    if (user.email.value && userEmailIsValid) {
      await authCtx.resetPassword(user.email.value);
      Alert.alert(
        'E-mail enviado',
        'Um e-mail foi enviado para você resetar sua password.'
      );
    } else {
      Alert.alert(
        'Dados incorretos',
        'Por favor, verifique se o campo e-mail foi digitado corretamente.'
      );
    }
  }

  // const formIsInvalid = !user.email.isValid || !user.password.isValid;

  return (
    <ImageBackground
      style={styles.authScreenContainer}
      source={require('../../assets/screen1.png')}
      resizeMode='stretch'
    >
      <Text style={styles.title}>{GLOBALS.APP.NAME}</Text>
      <KeyboardAvoidingView behavior='height' style={styles.container}>
        <View style={styles.formContainer}>
          <View style={styles.fieldContainer}>
            <Ionicons
              style={{ position: 'absolute', top: 8, left: 3, zIndex: 1 }}
              name='mail'
              size={24}
              color={GlobalStyles.colors.secondary}
            />
            <TextInput
              style={[styles.input, !user.email.isValid && styles.invalidInput]}
              // label='E-mail'
              placeholder='e-mail'
              value={user.email.value}
              onChangeText={emailChangeHandler}
            />
          </View>
          <View style={styles.fieldContainer}>
            <Ionicons
              style={{ position: 'absolute', top: 8, left: 3, zIndex: 1 }}
              name='key'
              size={24}
              color={GlobalStyles.colors.secondary}
            />
            <TextInput
              style={[styles.input, !user.password.isValid && styles.invalidInput]}
              placeholder='senha'
              label='Password'
              secureTextEntry={!showPassword}
              value={user.password.value}
              isValid={user.password.isValid}
              onChangeText={passwordChangeHandler}
            />
            <Ionicons
              style={{
                position: 'absolute',
                top: 8,
                right: 3,
                zIndex: 1,
                opacity: 0.4,
              }}
              name='eye'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => setShowPassword(!showPassword)}
            />
            {showPassword && (
              <Ionicons
                style={{
                  position: 'absolute',
                  top: 8,
                  right: 3,
                  zIndex: 1,
                  opacity: 0.4,
                }}
                name='eye-off'
                size={24}
                color={GlobalStyles.colors.secondary}
                onPress={() => setShowPassword(!showPassword)}
              />
            )}
          </View>
          <Text style={styles.forgetPassword} onPress={forgetPasswordHandler}>
            Esqueci minha senha
          </Text>
          <View style={styles.buttonsContainer}>
            <View styles={styles.buttonContainer}>
              <PrimaryButton onPress={signinHandler}>Login</PrimaryButton>
            </View>
          </View>
          <View styles={styles.buttonContainer}>
            <PrimaryButton onPress={signupHandler} style={styles.button}>
              Cadastre-se
            </PrimaryButton>
          </View>
        </View>
        {isAuthenticating && <Spinner />}
        <Image style={styles.image} source={require('../../assets/logo.png')} />
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}

export default AuthScreen;

const styles = StyleSheet.create({
  authScreenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: GlobalStyles.colors.primary,
  },
  formContainer: {
    width: '90%',
    // backgroundColor:'red'
  },
  fieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    width: '90%',
    // backgroundColor: 'white',
    marginBottom: 15,
  },
  input: {
    flex: 1,
    padding: 6,
    paddingLeft: 35,
    borderBottomColor: '#ddb52f',
    borderBottomWidth: 2,
    color: GlobalStyles.colors.textColor,
    // marginVertical: 8,
    fontSize: 18,
    fontWeight: 'bold',
    // textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 6,
  },
  buttonsContainer: {
    justifyContent: 'space-around',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    backgroundColor: 'transparent',
    color: GlobalStyles.colors.secondary,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 40,
    color: GlobalStyles.colors.secondary,
  },
  forgetPassword: {
    marginTop: -15,
    marginRight: 15,
    marginBottom: 20,
    textAlign: 'right',
    color: GlobalStyles.colors.secondary,
  },
  image: {
    // backgroundColor: 'red',
    alignSelf: 'center',
  },
  invalidLabel: {
    color: GlobalStyles.colors.error,
  },
  invalidInput: {
    backgroundColor: GlobalStyles.colors.error,
    opacity: 0.5,
  },
});
