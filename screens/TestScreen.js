import { View, Text, TextInput, StyleSheet } from 'react-native';
import { GlobalStyles } from '../constants/styles';
import Input from '../components/Input';

function UserScreen() {
  return (
    <View>
      <Text>UserScreen</Text>
      <View style={styles.container}>
        <View style={styles.box1}>
          <Text>Box1</Text>
        </View>
        <View style={styles.box2}>
          <Text>Box2</Text>
        </View>
        <View style={styles.box3}>
          <Text>Box3</Text>
        </View>
        <View style={styles.box4}>
          <Text>Box4</Text>
        </View>
        <View style={styles.box5}>
          <Text>Box5</Text>
        </View>
      </View>
    </View>
  );
}

export default UserScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: 'black',
  },
  box1: {
    backgroundColor: 'red'
  },
  box2: {
    backgroundColor: 'yellow'
  },
  box3: {
    backgroundColor: 'green'
  },
  box4: {
    backgroundColor: 'blue'
  },
  box5: {
    backgroundColor: 'black'
  },
});
