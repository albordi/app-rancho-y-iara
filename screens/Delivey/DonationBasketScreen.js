import { useContext, useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { ProductsContext } from '../../store/context/productscontext';
import { getCollection } from '../../util/firebase';
import ProductList from '../../components/ProductList';
import { DeliveryContext } from '../../store/context/deliverycontext';
import Spinner from '../../components/Spinner';
import { Label2 } from '../../components/UI/Labels';
function DonationBasketScreen() {
  console.log('[Donation Basket Screen] started ');
  const [isFetchingProducts, setIsFetchingProducts] = useState(false);
  const [error, setError] = useState(null);

  const deliveryCtx = useContext(DeliveryContext);

  // console.log('[Donation basket products] delivery', JSON.stringify(deliveryCtx.delivery, null, 2));

  function onChangeMaxProdHandle(productId, value) {
    console.log('[Donation Delivery Screen] increase quantity', productId);
    deliveryCtx.setMaxProd('Donation', productId, value);
  }

  return (
    <View style={StyleSheet.donationBasketScreenContainer}>
      <Label2>Escolha os itens da cesta de doação</Label2>
      {deliveryCtx.delivery.donationBasketProducts <= 0 ? (
        <Spinner />
      ) : (
        // <Text>Doação</Text>
        <ProductList
          products={deliveryCtx.delivery.donationBasketProducts}
          onChangeMaxProdHandle={onChangeMaxProdHandle}
        />
      )}
    </View>
  );
}

export default DonationBasketScreen;

const styles = StyleSheet.create({
  donationBasketScreenContainer: {
    backgroundColor: 'white',
  },
});
