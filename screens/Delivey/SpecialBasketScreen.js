import { useState, useEffect, useLayoutEffect ,useContext } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ProductList from '../../components/ProductList';
import { DeliveryContext } from '../../store/context/deliverycontext';

import { Label2 } from '../../components/UI/Labels';
import Spinner from '../../components/Spinner';

function SpecialBasketScreen() {
  console.log('[Special Basket Screen] started');
  const [isFetchingProducts, setIsFetchingProducts] = useState(false);
  const deliveryCtx = useContext(DeliveryContext);

  function onChangeMaxProdHandle(productId, value) {
    console.log('[Donation Delivery Screen] increase quantity', productId);
    deliveryCtx.setMaxProd('Special', productId, value);
  }

  return (
    <View style={styles.container}>
      <Label2>Escolha os itens da cesta especial</Label2>
      {deliveryCtx.delivery.donationBasketProducts.length > 0 ? (
        <ProductList
          products={deliveryCtx.delivery.donationBasketProducts}
          onChangeMaxProdHandle={onChangeMaxProdHandle}
        />
      ) : (
        <Spinner />
      )}
    </View>
  );
}

export default SpecialBasketScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
});
