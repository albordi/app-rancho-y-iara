import { useContext } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { DeliveryContext } from '../../store/context/deliverycontext';

import ProductList from '../../components/ProductList';
import Spinner from '../../components/Spinner';
import PrimaryButton from '../../components/PrimaryButton';
function ExtraProductsBasketScreen() {
  console.log('[Extra Products Basket Screen] started');
  const deliveryCtx = useContext(DeliveryContext);

  function onChangeMaxProdHandle(productId, value) {
    console.log('[Donation Delivery Screen] increase quantity', productId);
    deliveryCtx.setMaxProd('Special', productId, value);
  }
  return (
    <View style={styles.container}>
      {deliveryCtx.delivery.extraProducts <= 0 ? (
        <Spinner />
      ) : (
        <View>
          <ProductList
            products={deliveryCtx.delivery.extraProducts}
            onChangeMaxProdHandle={onChangeMaxProdHandle}
          />
          <PrimaryButton>Criar entrega</PrimaryButton>
        </View>
      )}
    </View>
  );
}

export default ExtraProductsBasketScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
});
