import React, { useState, useEffect, useContext } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import {
  View,
  Text,
  StyleSheet,
  Pressable,
  TextInput,
  TouchableWithoutFeedback,
  useCallback,
  Alert,
} from 'react-native';
import { getAuth } from 'firebase/auth';
import { format } from 'date-fns';
import { Ionicons } from '@expo/vector-icons';
import Input from '../../components/Input';
import GLOBALS from '../../Globals';
import DropDownInput from '../../components/DropDownInput';
import Label, { Label2 } from '../../components/UI/Labels';
import DateTimePicker from '@react-native-community/datetimepicker';
import ProductList from '../../components/ProductList';
import { ProductsContext } from '../../store/context/productscontext';
import { DeliveryContext } from '../../store/context/deliverycontext';
import { isValidDate } from '../../util/validation';
import { getFormattedDate } from '../../util/validation';
import { getCollection } from '../../util/firebase';

import { GlobalStyles } from '../../constants/styles';
import PrimaryButton from '../../components/PrimaryButton';
import Spinner from '../../components/Spinner';
import ErrorOverlay from '../../components/ErrorOverlay';
import AsyncStorage from '@react-native-async-storage/async-storage';

function ManageDeliveryScreen({ navigation }) {
  console.log('[Manage Delivery Screen] started.');
  const productsCtx = useContext(ProductsContext);
  const deliveryCtx = useContext(DeliveryContext);
  console.log(
    '[Manage Delivery Screen] delivery',
    JSON.stringify(deliveryCtx.delivery, null, 2)
  );
  // const [deliveryInputs, setDeliveryInputs] = useState({
  //   id: { value: 'd1', isValid: true },
  //   message: { value: 'xxx', isValid: true },
  //   deliveryDate: { value: '30/02/2023', isValid: true },
  //   normalBasketProducts: { value: [], isValid: true },
  //   extraProducts: { value: [], isValid: true },
  //   specialBasket: { value: [], isValid: true },
  //   productInAbundance: { value: 'p1', isValid: true },
  // });
  const [showDateTimePicker, setShowDateTimePicker] = useState(false);
  const [deliveryDate, setDeliveryDate] = useState({
    value: '',
    isValid: true,
  });
  const [deliveryMessage, setDeliveryMessage] = useState({
    value: '',
    isValid: true,
  });
  const [isFetchingProducts, setIsFetchingProducts] = useState(true);
  const [error, setError] = useState();

  // console.log('[Create Delivery Screen]', productCtx.products);

  useEffect(() => {
    // console.log('[Create Delivery Screen] useEffect ');
    async function getProducts() {
      setIsFetchingProducts(true);
      try {
        const products = await getCollection('products');
        deliveryCtx.setProducts(products);
      } catch (error) {
        // console.log('[Create Delivery Screen] error ', error);
        setError('Houve um erro ao carregar os produtos !!!');
      }
      setIsFetchingProducts(false);
    }
    getProducts();
  }, []);

  const showDateTimePickerHandler = () => {
    setShowDateTimePicker(!showDateTimePicker);
    console.log('Show Date Picker', showDateTimePicker);
  };

  // console.log('[Manage Delivery Screen] delivery Date',deliveryDate);

  const hideDatePicker = () => {
    setShowDateTimePicker(false);
  };

  const handleConfirm = (date) => {
    console.log('[Handle confirm]', new Date(date.nativeEvent.timestamp));
    hideDatePicker();
    const deliveryDateValue = format(
      new Date(date.nativeEvent.timestamp),
      GLOBALS.FORMAT.DEFAULT_DATE
    );

    deliveryDateChangeHandler(deliveryDateValue.toString());
  };

  const deliveryDateChangeHandler = (value) => {
    const deliveryDateIsValid = isValidDate(value);
    if (!deliveryDateIsValid) {
      Alert.alert('Formato da data inválido!', 'Digite a data novamente.');
      setDeliveryDate({ value: value, isValid: false });
      return;
    }
    // console.log(deliveryDateIsValid);
    setDeliveryDate({ value: value, isValid: true });
    deliveryCtx.setDeliveryDate({ value });
    console.log('[Manage Delivery Screen] delivery date changed');
  };

  const onSelectProdInAbundanceHandle = (value) => {
    console.log('[Manage Delivery Screen] onSelectProdinAbundance', value);
    deliveryCtx.setProductInAbundance(value);
  };

  // function createOrUpdateDeliveryObject() {
  //   const newDelivery = {
  //     message: deliveryInputs.message.value,
  //     deliveryDate: deliveryInputs.deliveryDate.value,
  //     normalBasketProducts: deliveryInputs.normalBasketProducts,
  //     extraProducts: deliveryInputs.extraProducts.value,
  //     specialBasket: deliveryInputs.specialBasket.value,
  //     productInAbundance: deliveryInputs.productInAbundance.value,
  //   };
  //   deliveryCtx.addDelivery(newDelivery);
  // }

  function onChangeMaxProdHandle(productId, value) {
    console.log('[Manage Delivery Screen] increase quantity', productId);
    deliveryCtx.setMaxProd('Normal', productId, value);
    // console.log(
    //   '[Manage Delivery Screen]',
    //   JSON.stringify(deliveryCtx.delivery, null, 2)
    // );
  }

  function onMessageChangeHandle(value) {
    console.log('value', value);
    deliveryCtx.setDeliveryMessage(value);
  }

  function errorHandler() {
    setError(null);
    navigation.goBack();
  }

  // console.log('[Create Delivery Screen] erro status', error, isFetchingProducts);

  if (error && !isFetchingProducts) {
    return <ErrorOverlay message={error} onConfirm={errorHandler} />;
  }

  return (
    <View style={styles.screenContainer}>
      <Label2>Texto de abertura</Label2>
      <View style={styles.messageContainer}>
        <Input
          style={styles.message}
          isValid={true}
          textInputConfig={{
            onChangeText: onMessageChangeHandle,
            value: deliveryCtx.message,
            placeholder: 'Digite aque o texto de abertura da entrega.',
            keyboardType: 'default',
          }}
        />
      </View>
      <Text style={styles.label}>Data da entrega</Text>
      <View style={styles.dateInputContainer}>
        <Input
          label='Data da Entrega'
          value={deliveryDate.value}
          isValid={deliveryDate.isValid}
          textInputConfig={{
            placeholder: 'DD-MM-YYYY',
            // maxLenght: "10",
            // autoCapitalize: 'none',
            // autoCorrect: false,
            onChangeText: deliveryDateChangeHandler,
          }}
        />
        <Ionicons
          style={{ position: 'absolute', top: 8, right: 3, zIndex: 1 }}
          name='calendar'
          size={24}
          color={GlobalStyles.colors.primary}
          onPress={showDateTimePickerHandler}
        />
      </View>
      {showDateTimePicker && (
        <DateTimePicker
          titleStyle={{ color: 'green', backgroundColor: 'red' }}
          value={new Date()}
          mode='date'
          display='spinner'
          minimumDate={new Date()}
          isVisible={showDateTimePicker}
          onChange={handleConfirm}
          onCancel={hideDatePicker}
          positiveButton={{
            label: 'Confirmar',
            textColor: GlobalStyles.colors.textColor,
          }}
          negativeButton={{
            label: 'Cancelar',
            textColor: GlobalStyles.colors.textColor,
          }}
          backGroundColor='black'
        />
      )}
      {isFetchingProducts ? (
        <Spinner />
      ) : (
        <View>
          <Label2>Produto em abundância</Label2>
          <View style={styles.dropDownInput}>
            {isFetchingProducts ? (
              <Spinner />
            ) : (
              <DropDownInput
                label='Produto em abundância'
                itemsToShow={deliveryCtx.delivery.normalBasketProducts}
                onSelectItemHandle={onSelectProdInAbundanceHandle}
              />
            )}
          </View>

          <Text style={styles.label}>
            Selecione os produtos que poderão estar na cesta
          </Text>
          <View style={styles.productListContainer}>
            <ProductList
              products={deliveryCtx.delivery.normalBasketProducts}
              onChangeMaxProdHandle={onChangeMaxProdHandle}
            />
            {isFetchingProducts &&
            deliveryCtx.delivery.normalBasketProducts.length <= 0 ? (
              <Label style={styles.text}>
                Não existem produtos cadastrados
              </Label>
            ) : null}
          </View>
        </View>
      )}

      {/* <View style={styles.buttonContainer}>
        <PrimaryButton>Criar Entrega</PrimaryButton>
      </View> */}
    </View>
  );
}

export default ManageDeliveryScreen;

const styles = StyleSheet.create({
  screenContainer: {
    paddingTop: 10,
    // flexDirection: 'row',
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: "center",
    // backgroundColor: GlobalStyles.colors.backGroundColor,
    // overflow: "hidden",
  },
  messageContainer: {
    marginHorizontal: 15,
    height: 120,
  },
  message: {
    height: 100,
    textAlign: 'left',
    textAlignVertical: 'top',
  },
  label: {
    color: GlobalStyles.colors.secondary,
    textAlign: 'left',
    marginLeft: 20,
    zIndex: -10,
  },
  dropDownInput: {
    // marginTop: 10,
    marginHorizontal: 20,
    marginBottom: 15,
  },
  dateInputContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginBottom: 10,
    zIndex: -1,
  },

  productListContainer: {
    zIndex: -1,
    height: '62%',
  },
  buttonContainer: {
    // backgroundColor: 'red',
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    paddingVertical: 5,
    alignSelf: 'center',
    // flexDirection: 'row',
    width: '80%',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginHorizontal: 20,
    zIndex: -1,
  },
  text: {
    textAlign: 'center',
  },
});
