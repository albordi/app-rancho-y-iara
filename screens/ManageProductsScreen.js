import { useContext, useEffect, useLayoutEffect, useState } from 'react';
import { View, Text, StyleSheet, Image, Pressable, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import firebase from 'firebase/compat/app';
import { isValidUrl } from '../util/validation';
import Input from '../components/Input';
import PrimaryButton from '../components/PrimaryButton';
import { GlobalStyles } from '../constants/styles';
import { ProductsContext } from '../store/context/productscontext';
import { Label1 } from '../components/UI/Labels';
import {
  addDocument,
  updateDocument,
  deleteDocument,
  getDocument,
} from '../util/firebase';
import Spinner from '../components/Spinner';
import ErrorOverlay from '../components/ErrorOverlay';

function ManageProductsScreen({ route, navigation }) {
  console.log('[Manage Products Screen] started');

  const [imageUri, setImageUri] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState(null);
  const [productInputs, setProductInputs] = useState({
    name: { value: '', isValid: true },
    description: { value: '', isValid: true },
    price: { value: '', isValid: true },
    imageUrl: { value: '', isValid: true },
  });

  // const [isUpdating, setIsUpdating] = useState();
  const productsCtx = useContext(ProductsContext);

  // For update purposes
  const productId = route.params?.productId;
  // let isUpdating = !!productId;

  // const productToUpdate = productsCtx.products.find(
  //   (product) => product.id === productId
  // );

  console.log('[Manage Products Screen] ', productId);
  // console.log('[Manage Products Screen] Product To Update', productToUpdate);

  // : {
  //   name: { value: 'Abacate', isValid: true },
  //   description: { value: null, isValid: true },
  //   price: { value: '10', isValid: true },
  //   availableQuantity: {
  //     value: 0,
  //     isValid: true,
  //   },
  //   imageUrl: {
  //     value:
  //       'https://st.depositphotos.com/1020804/1373/i/600/depositphotos_13739284-stock-photo-avocado-isolated-on-a-white.jpg',
  //     isValid: true,
  //   },
  // }

  // console.log('[Manage Products Screen] Product', productInputs);

  useLayoutEffect(() => {
    setIsLoading(true);
    const getProduct = async () => {
      const productToUpdate = await getDocument('products', productId);
      setProductInputs({
        name: { value: productToUpdate.name, isValid: true },
        description: { value: productToUpdate.description, isValid: true },
        price: { value: productToUpdate.price, isValid: true },
        imageUrl: { value: productToUpdate.imageUrl, isValid: true },
      });
      console.log('[Manage Product Screen] product to update', productToUpdate);
      setIsLoading(false);
    };
    productId ? getProduct() : setIsLoading(false);
  }, [productId]);

  const imageName = new Date().toString();

  useLayoutEffect(() => {
    navigation.setOptions({
      title: productId ? 'Atualiza produto' : 'Adiciona Produto',
    });
  }, [navigation, productId]);

  function nameChangeHandler(value) {
    // console.log('Product name changed', productInputs);
    const newProduct = {
      ...productInputs,
      name: { value: value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function descriptionChangeHandler(value) {
    // console.log('Product description changed', value);
    const newProduct = {
      ...productInputs,
      description: { value: value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function priceChangeHandler(value) {
    console.log('Product Price changed');
    const newProduct = {
      ...productInputs,
      price: { value: value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  // function availableQuantityChangeHandler(value) {
  //   console.log('Available Quantity changed');
  //   const newProduct = {
  //     ...productInputs,
  //     availableQuantity: { value: value, isValid: true },
  //   };
  //   setProductInputs(newProduct);
  // }

  // function imageChangeHandler(value) {
  //   console.log('Image changed');
  //   const newProduct = { ...productInputs, image: value };
  //   setProduct(newProduct);
  // }

  //Add or Update
  const confirmButtonHandler = async () => {
    setIsSubmitting(true);
    // console.log('[Manage Products Screen] Add Button pressed', product);
    const productNameIsValid = productInputs.name.value.trim().length > 0;
    const productPriceIsValid =
      !isNaN(productInputs.price.value) && productInputs.price.value >= 0;
    let productImageUrlIsValid = isValidUrl(productInputs.imageUrl.value);

    console.log(
      '[Manage Product Screen]',
      productNameIsValid,
      productPriceIsValid,
      productImageUrlIsValid
    );
    if (
      !productNameIsValid ||
      !productPriceIsValid ||
      !productImageUrlIsValid
    ) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setProductInputs({
        name: { value: productInputs.name.value, isValid: productNameIsValid },
        description: { value: productInputs.description.value, isValid: true },
        price: {
          value: productInputs.price.value,
          isValid: productPriceIsValid,
        },
        imageUrl: {
          value: productInputs.imageUrl.value,
          isValid: productImageUrlIsValid,
        },
      });
      setIsSubmitting(false);
      return;
    }

    const productToAddOrUpdate = {
      name: productInputs.name.value,
      description: productInputs.description.value,
      price: parseFloat(productInputs.price.value),
      imageUrl: productInputs.imageUrl.value,
    };

    if (productId) {
      try {
        await updateDocument('products', productId, productToAddOrUpdate);
        productsCtx.updateProduct(productId, productToAddOrUpdate);
      } catch (e) {
        console.log('Ocorreu um erro inesperado', e);
        setError('Ocorreu um erro inesperado', e);
      }
    } else {
      try {
        const id = await addDocument('products', productToAddOrUpdate);
        productsCtx.addProduct({ ...productToAddOrUpdate, id: id });
      } catch (e) {
        console.log('Ocorreu um erro inesperado', e);
        setError('Ocorreu um erro inesperado', e);
      }
    }
    setIsSubmitting(false);
    navigation.goBack();
  };

  const deleteProductHandler = async () => {
    setIsSubmitting(true);
    // setIsUpdating(false);
    console.log(
      '[Manage Products Screen] delete product handler function',
      productId
    );
    try {
      await deleteDocument('products', productId);
      // productsCtx.removeProduct(productId);
      navigation.goBack();
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
      setError('Ocorreu um erro inesperado', e);
      setIsSubmitting(false);
    }
  };

  console.log(
    '[Manage Products Screen] product',
    JSON.stringify(productInputs, null, 2)
  );

  function cancelButtonHandler() {
    navigation.goBack();
  }

  const pickImage = async () => {
    console.log('[Manage Products Screen] pick image started');
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: 'Images', // We can specify whether we need only Images or Videos
      allowsEditing: true,
      aspect: [2, 2],
      quality: 0.5, // 0 means compress for small size, 1 means compress for maximum quality
      selectionLimit: 1,
    });
    setImageUri(result.assets[0].uri);
    const imageUri = result.assets[0].uri;
    const response = await fetch(imageUri);
    const blob = await response.blob();
    const receiptRef = firebase.storage().ref().child(`${imageName}`);
    setIsLoading(true);
    receiptRef
      .put(blob)
      .then((response) => {
        receiptRef
          .getMetadata()
          .then((receiptMetadata) => {
            receiptRef
              .getDownloadURL()
              .then((url) => {
                setImageUrl(url);
                console.log(url);
                const newProduct = {
                  ...productInputs,
                  imageUrl: { value: url, isValid: true },
                };
                setProductInputs(newProduct);
                setIsLoading(false);
                return url;
              })
              .catch((error) => {
                console.log('Erro =>', error);
                Alert.alert(
                  'Erro ao recuperar o endereço da imagem do comprovante do banco de dados!',
                  error
                );
              });
          })
          .catch((error) => {
            Alert.alert(
              'Erro ao recuperar os metadados do comprovante de pagamento!',
              error
            );
          });
      })
      .catch((error) => {
        Alert.alert('Erro ao armazenar a imagem do comprovante!', error);
      });
  };

  const uploadImageToFirestoreStorage = async () => {
    const response = await fetch(imageUri);
    const blob = await response.blob();
    const receiptRef = firebase.storage().ref().child(`${imageName}`);
    setIsLoading(true);
    receiptRef
      .put(blob)
      .then((response) => {
        receiptRef
          .getMetadata()
          .then((receiptMetadata) => {
            receiptRef
              .getDownloadURL()
              .then((url) => {
                setImageUrl(url);
                console.log(url);
                const newProduct = { ...productInputs, imageUrl: url };
                setProductInputs(newProduct);
                setIsLoading(false);
                return url;
              })
              .catch((error) => {
                console.log('Erro =>', error);
                Alert.alert(
                  'Erro ao recuperar o endereço da imagem do comprovante do banco de dados!',
                  error
                );
              });
          })
          .catch((error) => {
            Alert.alert(
              'Erro ao recuperar os metadados do comprovante de pagamento!',
              error
            );
          });
      })
      .catch((error) => {
        Alert.alert('Erro ao armazenar a imagem do comprovante!', error);
      });
  };

  // console.log(
  //   '[Manage Products Screen]',
  //   JSON.stringify(productsCtx.products, null, 2)
  // );
  // console.log('[Manage Products Screen] imageUrl', imageUri);

  const formIsInvalid =
    !productInputs.name.isValid ||
    !productInputs.description.isValid ||
    !productInputs.price.isValid ||
    !productInputs.imageUrl.isValid;

  function errorHandler() {
    setError(null);
    navigation.goBack();
  }

  if (error && !isSubmitting) {
    return <ErrorOverlay message={error} onConfirm={errorHandler} />;
  }

  if (isSubmitting) {
    return <Spinner />;
  }

  return (
    <View style={styles.addNewProductScreenContainer}>
      <Text style={styles.title}>
        {productId ? 'Atualizar Produto' : 'Cadastrar Produto'}
      </Text>
      <Text style={styles.inputLabel}>Nome</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Nome do produto'
          value={productInputs.name.value}
          isValid={productInputs.name.isValid}
          textInputConfig={{
            onChangeText: nameChangeHandler,
            autoCapitalize: 'sentences',
          }}
        />
      </View>
      <Text style={styles.inputLabel}>Descrição</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Descrição do produto (Opcional)'
          value={productInputs.description.value}
          isValid={productInputs.description.isValid}
          textInputConfig={{
            onChangeText: descriptionChangeHandler,
            placeholder: 'Descrição do produto (Opcional)',
          }}
        />
      </View>
      <Text style={styles.inputLabel}>Preço</Text>

      <View style={styles.fieldContainer}>
        <Input
          label='Preço'
          value={productInputs.price.value.toString()}
          isValid={productInputs.price.isValid}
          textInputConfig={{
            onChangeText: priceChangeHandler,
            keyboardType: 'numeric',
          }}
        />
      </View>
      <Label1 style={{ textAlign: 'center' }}>Imagem</Label1>
      <View style={styles.imageContainer}>
        <Pressable onPress={pickImage}>
          <Ionicons name='cloud-upload' size={24} color='black' />
        </Pressable>
        {imageUri && (
          <Image
            source={{ uri: imageUri }}
            style={{ width: 100, height: 100 }}
          />
        )}
        <Text>{productInputs.imageUrl.value}</Text>
        {isLoading ? <Spinner /> : null}
      </View>
      {formIsInvalid && (
        <Text style={styles.errorText}>Campos inválidos !!!</Text>
      )}
      <View style={styles.buttonsContainer}>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={confirmButtonHandler}>
            {productId ? 'Atualizar' : 'Adicionar'}
          </PrimaryButton>
        </View>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={cancelButtonHandler}>Cancel</PrimaryButton>
        </View>
      </View>
      {productId && (
        <View style={styles.deleteIconContainer}>
          <Ionicons
            name='trash'
            size={36}
            color={GlobalStyles.colors.alert}
            onPress={deleteProductHandler}
          />
        </View>
      )}
    </View>
  );
}

export default ManageProductsScreen;

const styles = StyleSheet.create({
  addNewProductScreenContainer: {
    flex: 1,
    marginTop: 15,
    // backgroundColor: GlobalStyles.colors.primary,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 40,
    color: GlobalStyles.colors.secondary,
  },
  inputLabel: {
    color: GlobalStyles.colors.secondary,
    marginLeft: 20,
  },
  fieldContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
  },
  imageContainer: {
    // flex: 1,
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 20,
  },
  buttonContainer: {
    flex: 1,
  },
  deleteIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  errorText: {
    textAlign: 'center',
    color: GlobalStyles.colors.error,
    margin: 8,
  },
});
