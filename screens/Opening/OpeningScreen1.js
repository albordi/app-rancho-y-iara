import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Pressable,
} from 'react-native';
import { GlobalStyles } from '../../constants/styles';
import GLOBALS from '../../Globals';

function OpeningScreen1({ navigation }) {
  return (
    <ImageBackground
      style={styles.openingScreen1Container}
      source={require('../../assets/screen1.png')}
      resizeMode='stretch'
    >
      {/* <View style={styles.openingScreen1Container}> */}
      <Text style={styles.title}>{GLOBALS.APP.NAME}</Text>
      <Image
        // style={styles.tinyLogo}
        source={require('../../assets/logo.png')}
      />
      <Pressable
        android_ripple={{ color: GlobalStyles.colors.buttonHoverEffect }}
        onPress={() => navigation.navigate('AuthScreen')}
      >
        <Text style={styles.buttonText}>Entre</Text>
      </Pressable>
      {/* </View> */}
    </ImageBackground>
  );
}

export default OpeningScreen1;

const styles = StyleSheet.create({
  openingScreen1Container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    zIndex: 300,
  },
  title: {
    fontSize: 50,
    fontWeight: 'bold',
    textAlign: 'center',
    color: GlobalStyles.colors.secondary,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: GlobalStyles.colors.secondary,
  },
});
