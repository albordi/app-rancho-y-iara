export const GlobalStyles = {
  colors: {
    primary: '#f2c08d',
    secondary: '#8e2e30',
    buttonBackGroundColor: '#658a3c',
    tertiary: '#658a3c',
    alert: '#8e2e30',
    error: 'red',
    backGroundColorHeader: '#f2c08d',
    headerTitleColor: '#2d6535',
    backGroundColor: '#FFEEA8',
    buttonHoverEffect: '#96785E',
    textColor: '#753734',
    inactiveIconColor: '#C8D6E0',
    activeIconColor: '#FC6111',
  },
  standardLabel: {
    fontSize: 32,
    color: '#753734',
    textAlign: "left",
  },
};