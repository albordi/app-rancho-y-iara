import 'react-native-gesture-handler';
import { useCallback } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { StatusBar } from 'expo-status-bar';
import { Modal, StyleSheet, Text, View } from 'react-native';
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import { useFonts, Roboto_100Thin } from '@expo-google-fonts/roboto';

import * as SplashScreen from 'expo-splash-screen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Ionicons } from '@expo/vector-icons';

import { GlobalStyles } from './constants/styles';
import OpeningScreen1 from './screens/Opening/OpeningScreen1';
import DeliveryScreen from './screens/DeliveryScreen';
import AuthScreen from './screens/User/AuthScreen';
import SignupScreen from './screens/User/SignupScreen';
import UsersScreen from './screens/User/UsersScreen';
import UserScreen from './screens/User/UserScreen';

import ProductsScreen from './screens/ProductsScreen';
import ManageProductsScreen from './screens/ManageProductsScreen';
import ManageDeliveryScreen from './screens/Delivey/ManageDeliveryScreen';
import DonationBasketScreen from './screens/Delivey/DonationBasketScreen';
import SpecialBasketScreen from './screens/Delivey/SpecialBasketScreen';
import ExtraProductsBasketScreen from './screens/Delivey/ExtraProductsScreen';

import UserContextProvider from './store/context/usercontext';
import ProductsContextProvider from './store/context/productscontext';
import AuthContextProvider, { AuthContext } from './store/context/authcontext';
import DeliveryContextProvider from './store/context/deliverycontext';
import { useContext } from 'react';

// SplashScreen.preventAutoHideAsync();

const Stack = createNativeStackNavigator();
const BottomTabs = createBottomTabNavigator();
const Tab = createMaterialTopTabNavigator();

function CreateDeliveryTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        labelStyle: {
          fontSize: 12, // Change the font size of the tab labels
        },
        activeTintColor: GlobalStyles.colors.secondary, // Change the font color of the active tab label
        inactiveTintColor: 'gray', // Change the font color of the inactive tab labels
      }}
    >
      <Tab.Screen name='Criar Entrega' component={ManageDeliveryScreen} />
      <Tab.Screen name='Cesta Doação' component={DonationBasketScreen} />
      <Tab.Screen name='Cesta Especial' component={SpecialBasketScreen} />
      <Tab.Screen
        name='Produtos Extras'
        component={ExtraProductsBasketScreen}
      />
    </Tab.Navigator>
  );
}

function AuthStack() {
  return (
    <Stack.Navigator
    // screenOptions={{
    //   headerStyle: { backgroundColor: Colors.primary500 },
    //   headerTintColor: 'white',
    //   contentStyle: { backgroundColor: Colors.primary100 },
    // }}
    >
      <Stack.Screen
        name='OpeningScreen1'
        component={OpeningScreen1}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='AuthScreen'
        component={AuthScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SignupScreen'
        component={SignupScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

function AuthenticatedStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
        },
        headerTintColor: GlobalStyles.colors.secondary,
      }}
    >
      <Stack.Screen
        name='drawer'
        component={DrawerNav}
        options={{ headerShown: false }} // hide the header
      />
      <Stack.Screen
        name='ManageDeliveryScreen'
        component={ManageDeliveryScreen}
      />
      <Stack.Screen name='UserScreen' component={UserScreen} />
      {/* <Stack.Screen
      name='UserBottomTab'
      component={UserBottomTab}
      options={{ headerShown: false }}
    /> */}
      <Stack.Screen name='Deliveries' component={DeliveryScreen} />
      <Stack.Screen
        name='ManageProductsScreen'
        component={ManageProductsScreen}
        options={{
          title: 'Adicionar Produto',
          // presentation: 'modal'
        }}
      />
    </Stack.Navigator>
  );
}

function Navigation() {
  const authCtx = useContext(AuthContext);

  return (
    <NavigationContainer>
      {!authCtx.isAuthenticated && <AuthStack />}
      {authCtx.isAuthenticated && <AuthenticatedStack />}
      {/* <DrawerNav />
      <AuthenticatedStack /> */}
    </NavigationContainer>
  );
}

function UserBottomTab() {
  return (
    <BottomTabs.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
        },
        headerTintColor: GlobalStyles.colors.secondary,
        tabBarStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
          tabBarActiveTintColor: GlobalStyles.colors.secondary,
        },
      }}
    >
      <BottomTabs.Screen
        name='DeliveriesScreen'
        component={DeliveryScreen}
        options={{
          title: 'Entregas',
          tabBarLabel: 'Entregas',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name='md-basket' size={size} color={color} />
          ),
        }}
      />
      <BottomTabs.Screen
        name='UserScreen'
        component={UserScreen}
        options={{
          title: 'Detalhes da Pessoa Consumidora',
          tabBarLabel: 'Consumidor',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name='person' size={size} color={color} />
          ),
        }}
      />
    </BottomTabs.Navigator>
  );
}

function CustomDrawerContent(props) {
  const authCtx = useContext(AuthContext);

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem label='Sair' onPress={() => authCtx.signout()} />
    </DrawerContentScrollView>
  );
}

const Drawer = createDrawerNavigator();

function DrawerNav() {
  return (
    <Drawer.Navigator
      screenOptions={{
        // headerShown:false
        headerStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
        },
        headerTitleStyle: {
          color: GlobalStyles.colors.secondary,
        },
        headerTintColor: GlobalStyles.colors.secondary,
        drawerActiveTintColor: GlobalStyles.colors.secondary,
        drawerInactiveTintColor: GlobalStyles.colors.secondary,
        drawerStyle: {
          backgroundColor: GlobalStyles.colors.primary,
          width: 240,
        },
      }}
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >

      <Drawer.Screen
        name='Criar Nova Entrega'
        component={CreateDeliveryTabs}
        label='Criar Entrega'
      />
      <Drawer.Screen name='Gerenciar Produtos' component={ProductsScreen} />
      <Drawer.Screen name='Gerenciar Usuários' component={UsersScreen} />

      <Drawer.Screen name='Fazer Pedido' component={UsersScreen} />
      {/* <Drawer.Screen name='Logout' component={() => authCtx.signout()} /> */}
      {/* <Drawer.Screen name="Logout" component={() => console.log('Drawer')} /> */}
    </Drawer.Navigator>
  );
}

export default function App() {
  console.log('[App] app started');
  let [fontsLoaded] = useFonts({ Roboto_100Thin });

  // const onLayoutRootView = useCallback(async () => {
  //   if (fontsLoaded) {
  //     await SplashScreen.hideAsync();
  //   }
  // }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }

  return (
    <>
      <StatusBar style='auto' />
      <UserContextProvider>
        <AuthContextProvider>
          <ProductsContextProvider>
            <DeliveryContextProvider>
              <Navigation />
            </DeliveryContextProvider>
          </ProductsContextProvider>
        </AuthContextProvider>
      </UserContextProvider>
    </>
  );
}

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: "#fff",
  //   alignItems: "center",
  //   justifyContent: "center",
  // },
  // rootScreen: {
  //   // flex: 1,
  //   backgroundColor: GlobalStyles.colors.backGroundColor,
  // },
});
