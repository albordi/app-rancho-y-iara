import {
  collection,
  addDoc,
  getDocs,
  doc,
  deleteDoc,
  updateDoc,
  setDoc,
  getDoc
} from 'firebase/firestore';
import { db } from '../firebaseConfig';

export async function addDocument(collectionToStore, document) {
  console.log('[Firebase function storeData]', collectionToStore, document);
  const docRef = await addDoc(collection(db, collectionToStore), document);
  console.log('Document written with ID: ', docRef.id);
  return docRef.id;
}

export async function addDocumentWithCustomId(collectionToStore, document, docId) {
  console.log('[Firebase function storeData]', collectionToStore, document, docId);
  await setDoc(doc(db, collectionToStore, docId), document);
  console.log('[Firebase function storeData] Document written with ID: ', docId);
  return;
}

export async function getCollection(collectionToGet) {
  const response = [];
  const querySnapshot = await getDocs(collection(db, collectionToGet));
  querySnapshot.forEach((doc) => {
    console.log(`${doc.id} => ${doc.data()}`);
    response.push({ id: doc.id, ...doc.data() });
  });
  // console.log('[Firebase function getData]', JSON.stringify(response, null, 2));
  return response;
}

export async function getDocument(collectionToGet, id) {
  try {
    console.log('[Fireabse util module] getting document', id)
    const response = await getDoc(doc(db, collectionToGet, id));
    console.log('[Firebase component] Document read with ID: ', response.data());
    return response.data();
  } catch (e) {
    console.error('Error getting document: ', e);
  }
}

export async function updateDocument(collectionToUpdate, id, data) {
  try {
    const docRef = doc(db, collectionToUpdate, id);
    await updateDoc(docRef, data);
    console.log('Document updated with ID: ', id);
  } catch (e) {
    console.error('Error updating document: ', e);
  }
}

export async function deleteDocument(collectionToDelete, id) {
  try {
    await deleteDoc(doc(db, collectionToDelete, id));
    console.log('[Firebase component] Document deleted with ID: ', id);
  } catch (e) {
    console.error('Error deleting document: ', e);
  }
}