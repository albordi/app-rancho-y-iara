export function getFormattedDate(date) {
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
}

export function getDateMinusDays(date, days) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - days);
}

export const isValidUrl = (urlString) => {
  var urlPattern = new RegExp(
    '^(https?:\\/\\/)?' + // validate protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // validate domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // validate OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // validate port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // validate query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  ); // validate fragment locator
  return !!urlPattern.test(urlString);
};

export const isValidPhoneNumber = (phoneString) => {
  console.log(phoneString);
  // var re = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
  let re = /^\s*(\d{2}|\d{0})[-. ]?(\d{5}|\d{4})[-. ]?(\d{4})[-. ]?\s*$/;

  return re.test(phoneString);
};

export const isValidEmail = (emailString) => {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailString)) {
    return true;
  }
  return false;
};

export const isValidDate = (dateString) => {
  console.log(dateString);
  

  const dateRegex = /^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0-2])\/\d{4}$/;
  // const dateString = '01/05/2023';

  if (!dateRegex.test(dateString)) {
    console.log('Invalid date!');
    return false;
  } 

  // // Parse the date string into a Date object
  // const date = new Date(dateString);

  // Check if the parsed date is a valid date
  // if (isNaN(date.getTime())) {
  //   return false;
  // }

  // // Check if the month and year values are valid
  // const month = date.getMonth() + 1;
  // const year = date.getFullYear();

  // if (month < 1 || month > 12 || year < 1000 || year > 3000) {
  //   return false;
  // }

  // // Check if the day value is valid for the given month and year
  // const daysInMonth = new Date(year, month, 0).getDate();
  // const day = date.getDate();

  // if (day < 1 || day > daysInMonth) {
  //   return false;
  // }

  // If all checks pass, the date is valid
  return true;
};
