import { useState, useEffect, createContext } from 'react';

const DELIVERIES = [
  {
    id: 'd1',
    message: '',
    deliveryDate: '30/03/2023',
    normalBasketProducts: [],
    donationBasketProducts: [],
    extraProducts: [],
    specialBasket: [],
    productInAbundance: 'p1',
  },
  {
    id: 'd2',
    deliveryDate: '10/02/2023',
    products: [],
    productInAbundance: 'p2',
  },
  {
    id: 'd3',
    deliveryDate: '10/10/2023',
    products: [],
    productInAbundance: 'p3',
  },
];

export const DeliveryContext = createContext({
  id: '',
  addDelivery: (delivery) => {},
  removeDelivery: (id) => {},
  updateDelivery: (id, delivery) => {},
});

function DeliveryContextProvider({ children }) {
  const [deliveries, setDeliveries] = useState([]);
  const [delivery, setDelivery] = useState({
    id: '',
    message: '',
    deliveryDate: new Date(),
    normalBasketProducts: [],
    donationBasketProducts: [],
    specialBasketProducts: [],
    extraProducts: [],
    productInAbundance: '',
  });

  useEffect(() => {
    setDeliveries(DELIVERIES);
  }, []);

  function addDelivery(delivery) {
    // setDelivery(delivery);
    // console.log('[Delivery Context] delivery',delivery);
    return id;
  }

  function setDeliveryMessage(message) {
    const newDelivery = { ...delivery, message: messge };
    console.log(
      '[Delivery Context] delivery',
      JSON.stringify(newDelivery, null, 2)
    );
    setDelivery(newDelivery);
  }

  function setDeliveryDate(date) {
    const newDelivery = { ...delivery, deliveryDate: date };
    setDelivery(newDelivery);
    console.log('[Delivery Context] delivery', delivery);
  }

  function setProducts(products) {
    const newProducts = [];
    products.map((product) => {
      newProduct = {
        id: product.id,
        name: product.name,
        imageUrl: product.imageUrl,
        availableProducts: 0,
      };
      newProducts.push(newProduct);
    });
    const newDelivery = {
      ...delivery,
      normalBasketProducts: [...newProducts],
      donationBasketProducts: [...newProducts],
      specialBasketProducts: [...newProducts],
      extraProducts: [...newProducts]
    };
    setDelivery(newDelivery);
    console.log('[Delivery Context] delivery', delivery);
  }

  function setProductInAbundance(product) {
    const newDelivery = { ...delivery, productInAbundance: product };
    setDelivery(newDelivery);
    console.log('[Delivery Context] delivery', JSON.stringify(delivery));
  }

  function setMaxProd(basket, productId, value) {
    const newDelivery = { ...delivery };
    console.log(productId);
    if (basket === 'Normal') {
      const index = newDelivery.normalBasketProducts.findIndex(
        (obj) => obj.id === productId
      );
      newDelivery.normalBasketProducts[index].availableProducts = value;
    }
    if (basket === 'Donation') {
      const index = newDelivery.donationBasketProducts.findIndex(
        (obj) => obj.id === productId
      );
      newDelivery.donationBasketProducts[index].availableProducts = value;
    }
    if (basket === 'Special') {
      const index = newDelivery.specialBasketProducts.findIndex(
        (obj) => obj.id === productId
      );
      newDelivery.specialBasketProducts[index].availableProducts = value;
    }
    setDelivery(newDelivery);
    console.log('[Delivery Context] increaseMaxProd');
    console.log('[Delivery Context] delivery', JSON.stringify(delivery));
  }

  // function setDonationBasketProducts(products) {
  //   // console.log('[Delivery Context] set donation basket products', products);
  //   const newProducts = [];
  //   products.map((product) => {
  //     newProduct = {
  //       id: product.id,
  //       name: product.name,
  //       imageUrl: product.imageUrl,
  //       availableProducts: 0,
  //     };
  //     newProducts.push(newProduct);
  //   });
  //   const newDelivery = {
  //     ...delivery,
  //     donationBasketProducts: [...newProducts],
  //   };
  //   console.log('[Delivery Context] set donation basket products', newDelivery);
  //   setDelivery(newDelivery);
  //   console.log('[Delivery Context] delivery', delivery);
  // }

  function removeDelivery(id) {
    return id;
  }

  function updateDelivery(id, delivery) {
    return id;
  }

  const value = {
    delivery: delivery,
    deliveries: deliveries,
    addDelivery: addDelivery,
    removeProduct: removeDelivery,
    updateProduct: updateDelivery,
    setDeliveryMessage: setDeliveryMessage,
    setDeliveryDate: setDeliveryDate,
    setProducts: setProducts,
    setProductInAbundance: setProductInAbundance,
    setMaxProd: setMaxProd,
  };

  return (
    <DeliveryContext.Provider value={value}>
      {children}
    </DeliveryContext.Provider>
  );
}

export default DeliveryContextProvider;
