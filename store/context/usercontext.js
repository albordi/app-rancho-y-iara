import { useState, createContext } from 'react';
import { collection, query, where, getDocs } from 'firebase/firestore';
import {
  addDocument,
  updateDocument,
  deleteDocument,
  getCollection
} from '../../util/firebase';
import { getAuth } from 'firebase/auth';

export const UserContext = createContext({
  user: {},
  users: [],
  fetchUser: (id) => {},
  fetchUsers: () => {},
  createUser: (user) => {},
  updateUser: (id, user) => {},
  deleteUser: (id) => {},
});

function UserContextProvider({ children }) {
  const [user, setUser] = useState({});
  const [users, setUsers] = useState([]);

  async function addUser(user) {}

  async function deleteUser(userId) {
    try{
      const response = await deleteDocument('users', userId);
      fetchUsers();
      
    } catch(error){
      console.log('[User context] delete user error', error)
    }
  }

  async function updateUser(userId, user) {}

  async function fetchUser(userId) {
    const docRef = doc(db, 'cities', 'SF');
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      console.log('Document data:', docSnap.data());
    } else {
      // doc.data() will be undefined in this case
      console.log('No such document!');
    }
  }

  async function fetchUsers() {
    console.log('[User Context] fetch users');
    try {
      const response = await getCollection('users');      
      // console.log('[User context] users', response);
      setUsers(response);
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
    }
  }

  const value = {
    user: user,
    users: users,
    fetchUser: fetchUser,
    fetchUsers: fetchUsers,
    addUser: addUser,
    updateUser: updateUser,
    deleteUser: deleteUser,
  };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
}

export default UserContextProvider;
